import React, { Component } from 'react';

export class TodoItem extends Component {
    getStyle = () => {
        return {
            fontStyle: 'oblique',
            fontSize: '25px',
            background: '#fff',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.todo.completed ? 'line-through' : 'none',
        }
    };

    render() {
        const { id, title, completed } = this.props.todo;
        return (
            <div style={ this.getStyle() }>
                <p>
                    <input type="checkbox" onChange={ this.props.markComplete.bind(this, id ) } checked={ completed ? 'checked': '' }/>{' '}
                    {title}
                    <button className= 'btn-del' onClick={this.props.delTodo.bind(this, id)} style={{ float: 'right' }}>
                        Delete
                    </button>
                </p>
            </div>
        )
    }
}


export default TodoItem;