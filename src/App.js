import React from 'react';
import './App.css';

import TodoItem from "./components/TodoItem";
import Todos from "./components/Todos";
import AddTodo from './components/AddTodo';

class App extends React.Component {
	constructor() {
        super()
        this.state = {
    		todos: []
        }
    }


    addTodo = (title) => {
      const newTodo = {
      	  key: title,
	      title: title,
	      completed: false
    }
    this.setState({ todos: [...this.state.todos, newTodo]})}


    delTodo = (id) => {
    	this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)]});
    }

	markComplete = (id) => {
	      this.setState({
	        todos: this.state.todos.map(todo => {
	          if(todo.id === id)
	            todo.completed = !todo.completed;
	          return todo;
	        })
	      });
	}



	render() {
	    return (
	    	<div>
	    	<React.Fragment>
	    	<AddTodo addTodo={this.addTodo} />
	        <Todos todos={this.state.todos} markComplete = {this.markComplete} delTodo={this.delTodo}/>
	        </React.Fragment>
	        </div>

	    );
	  }
	}

export default App;
